Source: jsonrpclib-pelix
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Tristan Seligmann <mithrandi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
Standards-Version: 4.6.0
Homepage: https://github.com/tcalmant/jsonrpclib/
Vcs-Git: https://salsa.debian.org/python-team/packages/jsonrpclib-pelix.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/jsonrpclib-pelix
Rules-Requires-Root: no

Package: python3-jsonrpclib-pelix
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: Implementation of the JSON-RPC v2.0 specification (Python 3)
 This library is an implementation of the JSON-RPC specification.
 It supports both the original 1.0 specification, as well as the
 new (proposed) 2.0 specification, which includes batch submission, keyword
 arguments, etc.
 .
 This is a patched version of the original ``jsonrpclib`` project.
 .
 The suffix *-pelix* only indicates that this version works with Pelix Remote
 Services, but it is **not** a Pelix specific implementation.
